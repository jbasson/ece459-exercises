fn sum_of_multiples(number: i32, multiple1: i32, multiple2: i32) -> i32
{
    let mut sum = 0;

    let mut current_multiple = multiple1;
    while current_multiple < number {
        sum += current_multiple;
        current_multiple += multiple1;
    }

    current_multiple = multiple2;
    while current_multiple < number {
        if current_multiple % multiple1 != 0 {
            sum += current_multiple;
        }

        current_multiple += multiple2;
    }

    sum
}

fn main() {
    println!("{}", sum_of_multiples(1000, 5, 3));
    println!("{}", sum_of_multiples(10, 5, 3));
}
