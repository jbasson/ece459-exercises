// You should implement the following function
fn fibonacci_number(n: u32) -> u32 {
    let mut fib_num = 0;
    if n == 1 {
        fib_num = 1;
    } else if n > 1 {
        let mut prev_fib_nums: [u32; 2] = [0, 1];

        for _ in 1..n {
            fib_num = prev_fib_nums[0] + prev_fib_nums[1];
            prev_fib_nums[0] = prev_fib_nums[1];
            prev_fib_nums[1] = fib_num;
        }
    } else {
        // nothing
    }

    fib_num
}


fn main() {
    println!("{}", fibonacci_number(10));
}
